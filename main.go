package main

import (
	"net/http"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"encoding/json"
	"log"
	"strconv"
	"github.com/gorilla/mux"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"context"
)

const dbKey = 1

type User struct {
	gorm.Model
	Name string `json:"name"`
	Email string `json:"email"`
	Mobile string `json:"mobile"`
	Gender string `json:"gender"`
	Activities []Activity `json:"activities"`
}

func dbFromRequest(r *http.Request) *gorm.DB {
	return r.Context().Value(dbKey).(*gorm.DB)
}

func userFromRequest(r *http.Request) (User, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return User{}, err
	}

	user := User{}
	err = json.Unmarshal(body, &user)
	return user, err
}

func R(data interface{}, w http.ResponseWriter) {
	b, err := json.Marshal(data)
	if err != nil {
		R(map[string]string{"status": "FAILED"}, w)
		return
	}
	w.Write(b)
}

func createUserHandler(w http.ResponseWriter, r *http.Request) {
	db := dbFromRequest(r)
	user, err := userFromRequest(r)
	if err != nil {
		log.Print(err)
		return
	}

	u := User{}
	err = db.Where("email = ?", user.Email).First(&u).Error
	if err == nil {
		log.Println("User already exists")
		R(map[string]interface{}{"status": "SUCCESS", "id": u.ID}, w)
		return
	}
	err = db.Save(&user).Error
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	defaultActivities := []Activity{
		Activity{Activity: "steps", Value: "0", DataType: "int", UserId: user.ID},
		Activity{Activity: "distance", Value: "0", DataType: "float", UserId: user.ID},
		Activity{Activity: "calorie", Value: "0", DataType: "float", UserId: user.ID},
	}

	for _, activity := range defaultActivities {
		err = db.Save(&activity).Error
		if err != nil {
			log.Println(err)
			R(map[string]string{"status": "FAILED"}, w)
			return
		}
	}
	R(map[string]string{"status": "SUCCESS", "id": strconv.Itoa(int(user.ID))}, w)
}

type Activity struct {
	gorm.Model
	Activity string `json:"activity"`
	DataType string `json:"dataType"`
	Value string `json:"value"`
	UserId uint `json:"user_id"`
}

func activityFromRequest(r *http.Request) (Activity, error) {
	activity := Activity{}
	vars := r.URL.Query()
	log.Println(vars)
	userId, err := strconv.Atoi(vars.Get("id"))
	if err != nil {
		return activity, err
	}
	activity.UserId = uint((userId))
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return activity, err
	}

	err = json.Unmarshal(b, &activity)
	return activity, err
}

func saveActivityHandler(w http.ResponseWriter, r *http.Request) {
	db := dbFromRequest(r)
	activity, err := activityFromRequest(r)
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	err = db.Save(&activity).Error
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	R(map[string]string{"status": "SUCCESS"}, w)
}

func getUserActivityHandler(w http.ResponseWriter, r *http.Request) {
	db := dbFromRequest(r)
	v := r.URL.Query()
	userId, err := strconv.Atoi(v.Get("id"))
	activityName := v.Get("activityName")
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	activities := Activity{}
	err = db.Order("updated_at DESC").Where("user_id = ? and activity = ?", userId, activityName).First(&activities).Error
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	R(activities, w)
}

func getUserActivity(db *gorm.DB, userId uint, activityName string) (Activity, error) {
	activities := Activity{}
	err := db.Order("updated_at DESC").Where("user_id = ? and activity = ?", userId, activityName).First(&activities).Error
	return activities, err
}

func getUserFriendsHandler(w http.ResponseWriter, r *http.Request) {
	users := []User{}
	db := dbFromRequest(r)
	err := db.Find(&users).Error
	if err != nil {
		log.Println(err)
		R(map[string]string{"status": "FAILED"}, w)
		return
	}

	friends := []map[string]interface{}{}
	for _, user := range users {
		steps, err := getUserActivity(db, user.ID, "steps")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			R(map[string]string{"status": "FAILED"}, w)
			return
		}

		distance, err := getUserActivity(db, user.ID, "distance")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			R(map[string]string{"status": "FAILED"}, w)
			return
		}

		calories, err := getUserActivity(db, user.ID, "calorie")
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			R(map[string]string{"status": "FAILED"}, w)
			return
		}

		friend := map[string]interface{}{ "name": user.Name, "id": user.ID, "activity": steps.Value, "distance": distance.Value, "calorie": calories.Value}
		friends = append(friends, friend)
	}
	R(friends, w)
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func Handler(db *gorm.DB, f http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(context.Background(), dbKey, db)
		r = r.WithContext(ctx)
		lrw := NewLoggingResponseWriter(w)
		f(lrw, r)
		log.Printf("%s %s [%d]\n", r.Method, r.URL.Path, lrw.statusCode)
	})
}

func main() {
	db, err := gorm.Open("sqlite3", "database.db")
	if err != nil {
		log.Println(err)
		return
	}

	db.AutoMigrate(&User{}, &Activity{})
	db.LogMode(true)

	r := mux.NewRouter()
	r.HandleFunc("/user", Handler(db, createUserHandler)).Methods("POST")
	r.HandleFunc("/user/activity", Handler(db, getUserActivityHandler)).Methods("GET")
	r.HandleFunc("/user/activity", Handler(db, saveActivityHandler)).Methods("POST")
	r.HandleFunc("/user/friends", Handler(db, getUserFriendsHandler)).Methods("GET")

	addr := ":8080"
	log.Println("Listening and serving at ", addr)
	log.Fatal(http.ListenAndServe(addr, r))
}

